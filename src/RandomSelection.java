import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;



public class RandomSelection {

	static WebDriver d;
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","D:\\Uday\\eclipse-workspace\\FingoShopFramwork\\resources\\chromedriver.exe");				
		//d = new FirefoxDriver();	
		d=new ChromeDriver();
		d.get("https://www.fingoshop.com/catalogsearch/result/?q=piston");
		//selectRandomProduct();
		getScreenShot("screen");
	}

	public static void selectRandomProduct(){

	    // Find and click on a random product
	    List<WebElement> allProducts = d.findElements(By.xpath("//div[@class='products-grid']/ul/li"));
	    Random rand = new Random();
	    int randomProduct = rand.nextInt(allProducts.size());
	    WebElement webElement = allProducts.get(randomProduct);
	    webElement.findElement(By.xpath("//div[@class='product-item-info']/div/div/a")).click();
	}
    public static void getScreenShot(String fileName) throws IOException{
        DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
        Date date = new Date();
        File scrFile = ((TakesScreenshot)d).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("E:\\selenium logs\\"+fileName+"-"+dateFormat.format(date)+".png"));
    }
	
}
